"""
volling URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from voting import views

urlpatterns = [
    path('main/', views.main, name='main'),
    path('login', views.login),
    
    path('sign_form/', views.sign_form, name='sign_form'),
    path('sign_agree/', views.sign_agree, name='sign_agree'),
    path('sign_up',views.sign_up),
    path('check_id', views.check_id),

    path('Basic', views.Basic, name='Basic'),
    path('index', views.index),
    path('vote_type', views.vote_type),
    path('client_register', views.client_register),

    path('logout', views.logout, name='logout'),
    
    path('client_list_result', views.client_list_result, name="client_list_result")
]
