from django.db import models

# Member 
class Member(models.Model):
    member_name=models.CharField(max_length=20)
    member_id=models.CharField(max_length=20)
    member_password=models.CharField(max_length=30)
    member_mobile=models.CharField(max_length=20)
    member_email=models.CharField(max_length=20)
    member_org=models.CharField(max_length=30) # Organization of member

# Vs type of voting
class VoteType01(models.Model):
    vote_id=models.CharField(max_length=20)
    vote_title=models.CharField(max_length=30)
    vote_checkStart=models.CharField(max_length=20)
    vote_checkOver=models.CharField(max_length=20)
    vote_option01=models.CharField(max_length=50) # Number 1 option to choose
    vote_option02=models.CharField(max_length=50) # Number 2 option to choose
    vote_countOption01=models.CharField(max_length=50) # Counts of 1 option
    vote_countOption02=models.CharField(max_length=50) # Counts of 2 option

class VoteType02(models.Model):
    vote_id=models.CharField(max_length=20)
    vote_title=models.CharField(max_length=30)
    vote_checkStart=models.CharField(max_length=20)
    vote_checkOver=models.CharField(max_length=20)
    vote_candidates=models.CharField(max_length=300) # List of candidates {'candidate1','candidate2',...}
    vote_candidatesCounts=models.CharField(max_length=200) #List of counts {'candidate1 counts','candidate2 counts2',...}

# Member to vote id 
class MemberToVote(models.Model):
    member_id=models.CharField(max_length=20)
    vote_id=models.CharField(max_length=20)

# Voter 
class Voter(models.Model):
    voter_id=models.CharField(max_length=20)
    voter_name=models.CharField(max_length=20)
    voter_mobile=models.CharField(max_length=20)
    voter_checkAuth=models.CharField(max_length=20) #Check authentication of voter
    voter_checkVoting=models.CharField(max_length=20) # Check voting or not

# Voter to VoteType
class VoterToVotetype(models.Model):
    voter_id=models.CharField(max_length=20)
    vote_id=models.CharField(max_length=20)

# Voter to Contract address
class VoterToCA:
    ca_address=models.CharField(max_length=200)
    voter_id=models.CharField(max_length=20)

