from django.shortcuts import render,redirect
from django.http import HttpResponse

from voting.models import Member
# Create your views here.
def index(request):
    return render(request,'voting/index.html',{})

def main(request):
        return render(request,'voting/main.html',{})
        
    
def login(request):
    if request.method == 'GET':
        return render(request,
                      'voting/main.html',
                      {}
                      )
    else:
        id = request.POST['id']
        password = request.POST['password']

        try:
            
            member = Member.objects.get(member_id=id, member_password=password)
            request.session['id'] = member.member_id

        except Member.DoesNotExist:
            return HttpResponse('Check your id and password again')
        return HttpResponse('Login success')
        
def logout(request):
    del request.session['id']
    return redirect('main')

def sign_form(request):
    
    if request.method =='GET':
        return render(request,'voting/sign_form.html',{})
    
    else:
        return render(request,'voting/sign_form.html',{})

def sign_up(request):
    if request.method =='GET':
        return render(request,'voting/sign_form.html',{})
    
    else:
        name = request.POST['name']
        id = request.POST['id']
        password = request.POST['password']
        mobile = request.POST['mobile']
        email = request.POST['email']
        org = request.POST['org']
        
        Member(member_name=name, member_id=id,
                member_password=password, member_mobile=mobile, member_email=email, member_org=org).save()

        return HttpResponse('success')


def sign_agree(request):
    return render(request,'voting/sign_agree.html',{})
    # return redirect('sign_agree')

def check_id(request):
    if request.method == 'GET':
        return render(request,
                      'voting/sign_form.html',
                      {}
                      )
    else:
        id=request.POST['id']

        try:
            Member.objects.get(member_id=id)
        except Member.DoesNotExist:
            return HttpResponse('available')
        except Member.MultipleObjectsReturned:
            return HttpResponse('MultipleObjectsReturned')
        return HttpResponse('Not available')

def Basic(request):
    return render(request,'voting/Basic.html',{})

def vote_type(request):
    return render(request,'voting/vote_type.html',{})

def client_register(request):
    return render(request,'voting/client_register.html',{})

def client_list_result(request):
    return render(request,'voting/client_list_result.html',{})